package encryption;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@XmlRootElement
@AllArgsConstructor
@NoArgsConstructor
public class XmlEncryptorElement {	
	@XmlElement
	public String type;
	@XmlElement
	public XmlEncryptorElement element1;
	@XmlElement
	public XmlEncryptorElement element2;
	
	public static Encryptor toEncryptor(EncryptorFactory factory,XmlEncryptorElement pojo){
		if (pojo == null){
			return null;
		}
		return factory.getEncryptor(pojo.type, toEncryptor(factory, pojo.element1),
				toEncryptor(factory, pojo.element2));
	}
	
	@Override
	public String toString(){
		if (element1!=null && element2 != null){
			return "type: "+type+" element1: "+element1.toString()+ " element2: "+element2.toString();
		}
		if (element1 != null){
			return "type: "+type+" element1: "+element1.toString();
		}
		return "type: "+type;
	}
	
	public static XmlEncryptorElement deseriallize(String path) throws JAXBException, SAXException{
		File file = new File(path);    
        JAXBContext jaxbContext = JAXBContext.newInstance(XmlEncryptorElement.class);    
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();    
        jaxbUnmarshaller.setSchema(getSchema());
        XmlEncryptorElement e=(XmlEncryptorElement) jaxbUnmarshaller.unmarshal(file);  
        return e;
	}
	
	private static Schema getSchema() throws SAXException{
		SchemaFactory sft = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        File schemaFile = new File(XmlEncryptorElement.class.getClassLoader().getResource("encryptor.xsd").getFile());
        return sft.newSchema(schemaFile);
	}
	
	public void seriallize(String xml) throws JAXBException, FileNotFoundException{
		JAXBContext contextObj = JAXBContext.newInstance(XmlEncryptorElement.class);  
	    Marshaller marshallerObj = contextObj.createMarshaller();  
	    marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);  
	    marshallerObj.marshal(this,new FileOutputStream(xml));  
	}
	
	public static String getDefaultXml(){
		return XmlEncryptorElement.class.getClassLoader().getResource("default.xml").getFile();
	}

}
