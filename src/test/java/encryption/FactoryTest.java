package encryption;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
public class FactoryTest {
	private final FileUtilsImp f = Mockito.mock(FileUtilsImp.class);
	
	@Test
	public void getPrintEncryptor(){
		EncryptorFactory factory = new EncryptorFactory(f);
		Encryptor encryptor = factory.getEncryptor(EncryptorFactory.CEASAR_ENCRYPTOR,null,null);
		assertNotNull(encryptor);
		boolean isPrint = encryptor instanceof CeasarEncryptor;
		assertTrue("encryptor is not peint encryptor",isPrint);;
	}
	@Test
	public void getNullOnIllegalInput(){
		EncryptorFactory factory = new EncryptorFactory(f);
		Encryptor encryptor = factory.getEncryptor("gibrish",null,null);
		assertNull(encryptor);
	}
}
