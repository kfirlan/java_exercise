package encryption;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import encryption.key.Key;
import encryption.key.KeyException;
import lombok.SneakyThrows;

public class AsynchDirEncryptor extends DirEncryptor{
	
	private ConcurrentLinkedQueue<File> tasks = new ConcurrentLinkedQueue<>();
	private Key key ;

	public AsynchDirEncryptor(FileUtils fileUtils, Encryptor encryptor) {
		super(fileUtils, encryptor);
	}
	
	@Override
	protected void encryptEachFile(File [] files,Key key) throws KeyException, IOException{
		doEachFile(files, key, ()->threadEncrypt());
	}
	@SneakyThrows
	private void threadEncrypt(){
		File tmp = tasks.poll();
		while (tmp != null){
			InputStream in = new FileInputStream(tmp);
			OutputStream out = new FileOutputStream(getEncryptedName(tmp.getAbsolutePath()));
			encrypt(in, out,key);
			tmp = tasks.poll();
		}
	}
	@SneakyThrows
	private void threadDecrypt(){
		File tmp = tasks.poll();
		while (tmp != null){
			InputStream in = new FileInputStream(tmp);
			OutputStream out = new FileOutputStream(getDecryptedName(tmp.getAbsolutePath()));
			decrypt(in, out,key );
			tmp = tasks.poll();
		}
	}
	
	@Override
	protected void decryptEachFile(File[] files,Key key) throws KeyException, IOException{
		doEachFile(files, key, ()->threadDecrypt());
	}
	
	private void doEachFile(File[] files,Key key,Runnable action){
		List<Thread> threads = new ArrayList<>();
		this.key = key;
		tasks.clear();
		threads.clear();
		for (File file : files) {
			tasks.add(file);
		}
		int workLeft = files.length;
		while (! tasks.isEmpty() && workLeft > 0){
			Thread t = new Thread(action);
			t.start();
			threads.add(t);
			workLeft--;
		}
		//wait all threads to finish
		threads.forEach((t)->sneakyJoin(t));
	}
	@SneakyThrows
	private void sneakyJoin(Thread t){
		t.join(); //just return if t already finished
	}
}
