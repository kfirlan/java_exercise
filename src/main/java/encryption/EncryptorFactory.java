package encryption;

import com.google.inject.Inject;

import lombok.Getter;
import lombok.NonNull;

public class EncryptorFactory {
	public static final String CEASAR_ENCRYPTOR = "ceasar";
	public static final String XOR_ENCRYPTOR = "xor";
	public static final String MWO_ENCRYPTOR = "mwo";
	public static final String DOUBLE_ENCRYPTOR = "double";
	public static final String REVERSE_ENCRYPTOR = "reverse";
	public static final String SPLIT_ENCRYPTOR = "split";
	@Getter
	@NonNull private final FileUtils fileUtils;
	Encryptor getEncryptor(@NonNull String encryptor,Encryptor e1,Encryptor e2){
		encryptor.toLowerCase();
		switch(encryptor){
		case CEASAR_ENCRYPTOR: return new CeasarEncryptor(fileUtils);
		case XOR_ENCRYPTOR: return new XorEncryptor(fileUtils);
		case MWO_ENCRYPTOR: return new MWOEncryptor(fileUtils);
		case DOUBLE_ENCRYPTOR: {
			DoubleEncryptor tmp =  new DoubleEncryptor(fileUtils);
			tmp.setEncryptor1(e1);
			tmp.setEncryptor2(e2);
			return tmp;
		}
		case REVERSE_ENCRYPTOR: {
			ReverseEncryptor tmp = new ReverseEncryptor(fileUtils);
			tmp.setEncryptor(e1);
			return tmp;
		}
		case SPLIT_ENCRYPTOR:{
			SplitEncryptor tmp = new SplitEncryptor(fileUtils);
			tmp.setEncryptor(e1);
			return tmp;
		}
		default: return null;
		}
	}
	
	DirEncryptor getDirEncryptor(Encryptor encryptor){
		return new DirEncryptor(fileUtils, encryptor);
	}
	
	AsynchDirEncryptor getAsynchDirEncryptor(Encryptor encryptor){
		return new AsynchDirEncryptor(fileUtils, encryptor);
	}
	@Inject
	public EncryptorFactory(FileUtils fileUtils) {
		this.fileUtils = fileUtils;
	}
}
