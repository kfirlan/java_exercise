package encryption;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.function.BiFunction;

import encryption.key.Key;
import encryption.key.KeyException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class Encryptor {
	
	@NonNull protected final FileUtils fileUtils;
	/**
	 * 
	 * @param file to encrypt
	 * @return the generated key
	 * @throws IOException 
	 */
	public Key encrypt(@NonNull String file,Key key) throws IOException{
		if (key == null){
			key = generateKey();
		}
		InputStream in = new FileInputStream(file);
		OutputStream out = new FileOutputStream(getEncryptedName(file));
		return encrypt(in, out,key);
	}
	
	/**
	 * 
	 * @param file to decrypt
	 * @param key key to decrypt with
	 * @return nothing
	 * @throws IOException 
	 * @throws KeyException 
	 */
	public void decrypt(@NonNull String file,String key) throws IOException{
		InputStream in = new FileInputStream(file);
		OutputStream out = new FileOutputStream(getDecryptedName(file));
		decrypt(in, out, Key.deseriallize(key));
	}
	/**
	 * encrypt input from in and write the result to out
	 * @param in
	 * @param out
	 * @return the key
	 * @throws IOException
	 * @throws KeyException 
	 */
	public abstract Key encrypt(@NonNull InputStream in,@NonNull OutputStream out,Key key) throws IOException, KeyException;
	/**
	 * decrypt input from in and write the result to out
	 * @param in
	 * @param out
	 * @param Key
	 * @throws IOException
	 * @throws KeyException 
	 */
	public abstract void decrypt(@NonNull InputStream in,@NonNull OutputStream out,Key key) throws IOException, KeyException;
	public abstract Key generateKey();
	
	public abstract XmlEncryptorElement toXml();
	
	public String getKeyPath(String filePath){
		int index = filePath.lastIndexOf('/');
		index = index > 0 ? index : 0;
		String keyPath = filePath.substring(0, index);
		keyPath =  keyPath+"/key.bin";
		return keyPath;
	}
	
	protected String getEncryptedName(String orgName){
		return orgName+".encrypted";
	}
	
	protected String getDecryptedName(String orgName){
		String file_name;
		String ext;
		if (orgName.lastIndexOf('.') == -1){
			file_name=orgName;
			ext="";
		}else {
			file_name= orgName.substring(0, orgName.lastIndexOf('.'));
			ext = "."+orgName.replaceAll("^.*\\.(.*)$", "$1");
		}
		file_name = file_name +"_decrypted"+ext;
		return file_name;
	}
	
	protected Key encryptEachByte(InputStream in,OutputStream out,BiFunction<Byte,Byte,Byte> encryption,Key key) throws IOException{
		byte[] cipher_text = fileUtils.read(in);
		byte bkey = key.getByteKey();
		for (int i = 0; i < cipher_text.length; i++) {
			cipher_text[i] = encryption.apply(cipher_text[i],bkey);
		}
		fileUtils.write(out, cipher_text);
		return key;
	}
	
	protected byte readKey(Key key){
		return key.getByteKey();
	}
	
	protected void decryptEachByte(InputStream in,OutputStream out,Key key,BiFunction<Byte,Byte,Byte> decryption) throws IOException, KeyException{
		byte keyVal = key.getByteKey();
		byte[] plain_text = fileUtils.read(in);
		for (int i = 0; i < plain_text.length; i++) {
			plain_text[i] = decryption.apply(plain_text[i], keyVal);
		}
		fileUtils.write(out, plain_text);
	}
}
