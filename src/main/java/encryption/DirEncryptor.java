package encryption;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import encryption.key.Key;
import encryption.key.KeyException;
import lombok.NonNull;

public class DirEncryptor extends Encryptor{
	
	private final Encryptor encryptor;
	private String encrypted;
	private String decrypted;

	public DirEncryptor(FileUtils fileUtils,Encryptor encryptor) {
		super(fileUtils);
		this.encryptor = encryptor;
	}
	
	@Override
	public Key encrypt(@NonNull String file,Key key) throws IOException{
		File[] files = getFiles(file);
		encrypted = file+"/encrypted";
		new File(encrypted).mkdir(); //create encrypted dir
		encryptEachFile(files,key);
		return key;
	}
	
	public void decrypt(@NonNull String file,String key) throws IOException{
		File[] files = getFiles(file);
		decrypted = file+"/decrypted";
		new File(decrypted).mkdir(); //create decrypted dir
		decryptEachFile(files, Key.deseriallize(key));
	}

	@Override
	public Key encrypt(InputStream in, OutputStream out, Key key) throws IOException, KeyException {
		return encryptor.encrypt(in, out, key);
	}

	@Override
	public void decrypt(InputStream in, OutputStream out, Key key) throws IOException, KeyException {
		encryptor.decrypt(in, out, key);		
	}

	@Override
	public Key generateKey() {
		return encryptor.generateKey();
	}
	
	@Override
	public String getKeyPath(String path){
		return path+"/key.bin";
	}
	
	private File[] getFiles(String file) throws IOException{
		File dir = new File(file);
		File[] files = dir.listFiles((x)->x.isFile());
		if (files == null){
			throw new IOException(file+" is not a dir");
		}
		return files;
	}
	
	@Override
	protected String getEncryptedName(String file){
		File tmp = new File(file);
		String name = tmp.getName();
		return encrypted+"/"+name;
	}
	
	@Override
	protected String getDecryptedName(String file){
		File tmp = new File(file);
		String name = tmp.getName();
		return decrypted+"/"+name;
	}
	
	protected void encryptEachFile(File [] files,Key key) throws KeyException, IOException{
		for (File subFile : files) {
			InputStream in = new FileInputStream(subFile);
			OutputStream out = new FileOutputStream(getEncryptedName(subFile.getAbsolutePath()));
			encrypt(in, out,key);
		}
	}
	
	protected void decryptEachFile(File[] files,Key key) throws KeyException, IOException{
		for (File subFile : files) {
			InputStream in = new FileInputStream(subFile);
			OutputStream out = new FileOutputStream(getDecryptedName(subFile.getAbsolutePath()));
			decrypt(in, out,key );
		}
	}

	@Override
	public XmlEncryptorElement toXml() {
		return null;
	}

}
