package encryption;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Test;
import org.mockito.Mockito;

import encryption.key.Key;

public class DoubleEncryptorTest extends TestWithPrint{
	@Test
	public void innerEncryptorWorks() throws IOException{
		FileUtils f = Mockito.mock(FileUtils.class);
		DoubleEncryptor encryptor = new DoubleEncryptor(f);
		Encryptor e1 = Mockito.mock(Encryptor.class);
		Encryptor e2 = Mockito.mock(Encryptor.class);
		InputStream in = Mockito.mock(InputStream.class);
		OutputStream out = Mockito.mock(OutputStream.class);
		Mockito.when(e1.generateKey()).thenReturn(new Key((byte) 1));
		Mockito.when(e2.generateKey()).thenReturn(new Key((byte) 1));
		encryptor.setEncryptor1(e1);
		encryptor.setEncryptor2(e2);
		encryptor.encrypt(in,out,encryptor.generateKey());
		Mockito.verify(e1).encrypt(Mockito.eq(in), Mockito.any(),Mockito.any());
		Mockito.verify(e2).encrypt(Mockito.any(),Mockito.eq(out),Mockito.any());
	}
	
	@Test 
	public void innerDecryptWorks() throws IOException{
		FileUtils f = Mockito.mock(FileUtils.class);
		DoubleEncryptor encryptor = new DoubleEncryptor(f);
		Encryptor e1 = Mockito.mock(Encryptor.class);
		Encryptor e2 = Mockito.mock(Encryptor.class);
		InputStream in = Mockito.mock(InputStream.class);
		OutputStream out = Mockito.mock(OutputStream.class);
		Key key1 = Mockito.mock(Key.class);
		Key key2 = Mockito.mock(Key.class);
		Key key = Mockito.mock(Key.class);
		Mockito.when(key.getFirstKey()).thenReturn(key1);
		Mockito.when(key.getSecondKey()).thenReturn(key2);
		encryptor.setEncryptor1(e1);
		encryptor.setEncryptor2(e2);
		encryptor.decrypt(in,out,key);
		Mockito.verify(e1).decrypt(Mockito.any(),Mockito.eq(out),Mockito.eq(key1));
		Mockito.verify(e2).decrypt(Mockito.eq(in),Mockito.any(),Mockito.eq(key2));
	}
}
