package encryption;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import encryption.key.Key;
import encryption.key.KeyException;
import lombok.NonNull;

public class XorEncryptor extends Encryptor{
	
	public XorEncryptor(@NonNull FileUtils fileUtils) {
		super(fileUtils);
	}
	
	@Override
	public Key generateKey() {
		byte[] res = new byte[1];
		new Random().nextBytes(res);
		return new Key(res[0]);
	}
	@Override
	public Key encrypt(InputStream in, OutputStream out,Key key) throws IOException {
		return encryptEachByte(in,out,(b,k)->(byte) (b^k),key);
	}
	@Override
	public void decrypt(InputStream in, OutputStream out, Key key) throws IOException, KeyException {
		decryptEachByte(in,out, key, (b,k)->(byte)(b^k));
	}

	@Override
	public XmlEncryptorElement toXml() {
		return new XmlEncryptorElement(EncryptorFactory.XOR_ENCRYPTOR, null, null);
	}

}
