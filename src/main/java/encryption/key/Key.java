package encryption.key;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import com.google.gson.Gson;

public class Key {
	Byte bKey = null;
	Key key1 = null;
	Key key2 = null;

	public Key(byte key) {
		this.bKey = key;
	}

	public Key(Key key1, Key key2) {
		this.key1 = key1;
		this.key2 = key2;
	}

	public byte getByteKey() throws KeyException {
		if (bKey == null) {
			throw new KeyException();
		}
		return bKey;
	}

	public Key getFirstKey() throws KeyException {
		if (key1 == null) {
			throw new KeyException();
		}
		return key1;
	}

	public Key getSecondKey() throws KeyException {
		if (key2 == null) {
			throw new KeyException();
		}
		return key2;
	}

	public static void seriallize(Key key,String path) throws IOException {
		FileOutputStream out = new FileOutputStream(path);
		out.write(Key.seriallize(key));
		out.close();
	}
	
	public static byte[] seriallize(Key key){
		return new Gson().toJson(key).getBytes();
	}

	public static Key deseriallize(String file) {
		try {
		FileInputStream fis = new FileInputStream(file);
		InputStreamReader isr = new InputStreamReader(fis);
		BufferedReader bufferedReader = new BufferedReader(isr);
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			sb.append(line);
		}

		String json = sb.toString();
		Gson gson = new Gson();
		Key key = gson.fromJson(json, Key.class);
		return key;
		}
		catch (IOException e){
			throw new KeyException();
		}
	}

}
