package encryption;

import java.io.IOException;

import org.junit.Test;
import org.mockito.Mockito;

import encryption.key.Key;

public class DirEncryptorTest extends TestWithPrint {
	
	@Test
	public void testEncryptCalled() throws IOException{
		Encryptor mockEncryptor = Mockito.mock(Encryptor.class);
		FileUtils fileUtils = Mockito.mock(FileUtils.class);
		folder.newFile("a1");
		folder.newFile("a2");
		Key key = Mockito.mock(Key.class);
		Encryptor encryptor = new DirEncryptor(fileUtils, mockEncryptor);
		encryptor.encrypt(folder.getRoot().getAbsolutePath(), key);
		Mockito.verify(mockEncryptor,Mockito.times(2)).encrypt(Mockito.any(), Mockito.any(),Mockito.any());
	}
}
