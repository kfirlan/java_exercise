package encryption;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class FileUtilsImplTest {
	@Rule
	public TemporaryFolder folder= new TemporaryFolder();
	
	@Test 
	public void writeToThenReadData() throws IOException{
		File f = folder.newFolder("sub");
		String path = f.getAbsolutePath()+"a.txt";
		File tmp = new File(path);
		if(!(!tmp.exists() || tmp.isDirectory())) { 
			assertTrue(false);// file should not exist
		}
		byte[] data = "123".getBytes();
		new FileUtilsImp().write(new FileOutputStream(tmp), data);
		tmp = new File(path);
		if(!tmp.exists() || tmp.isDirectory()) { 
			assertTrue(false); //file should exist
		}
		byte[] read = new FileUtilsImp().read(new FileInputStream(tmp));
		assertArrayEquals(data, read);
		
	}

}
