package encryption.key;

public class KeyException extends RuntimeException{

	public KeyException(){
		super();
	}
	public KeyException(String message) {
		super(message);
	}

}
