package encryption;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {

	public static void main(String[] args) throws IOException, JAXBException, SAXException {
		Injector injector = Guice.createInjector(new DefaultModule());
		UserInterface ui = injector.getInstance(UserInterface.class);
		ui.chooseAlgorithem();
		
	}
	

}
