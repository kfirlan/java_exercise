package encryption;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lombok.NonNull;

public interface FileUtils {
	public byte[] read(@NonNull InputStream in) throws IOException;
	public void write(@NonNull OutputStream out,@NonNull byte[] data)throws IOException;
}
