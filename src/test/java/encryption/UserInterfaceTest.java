package encryption;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.xml.sax.SAXException;

import encryption.key.KeyException;

import static org.junit.Assert.*;

public class UserInterfaceTest extends TestWithPrint {
	
	private Encryptor voidEncryptor = Mockito.mock(Encryptor.class);
	private EncryptorFactory voidFactory = Mockito.mock(EncryptorFactory.class);
	private TimerListener voidTimer = Mockito.mock(TimerListener.class);
	@Before
	public void setMock(){
		Mockito.when(voidFactory.getEncryptor(Mockito.anyString(),Mockito.any(),Mockito.any())).thenReturn(voidEncryptor);
	}
	
	@Test
	public void encryptInputCallEncryptMethod() throws IOException, JAXBException, SAXException{
		String path = folder.newFile("a_file").getAbsolutePath();
		addInput("1");
		addInput(path);
		endInput();
		Mockito.when(voidEncryptor.getKeyPath(Mockito.anyString())).thenReturn(folder.getRoot().getAbsolutePath() + "/key.bin");
		UserInterface ui = new UserInterface(voidFactory,voidTimer);
		ui.chooseAlgorithem();
		assertTrue(inContent.available() == 0);
		Mockito.verify(voidFactory).getEncryptor(Mockito.anyString(),Mockito.any(),Mockito.any());
		Mockito.verify(voidEncryptor,Mockito.times(1)).encrypt(Mockito.eq(path),Mockito.any());
	}
	
	@Test
	public void decryptInputCallDecryptMethod() throws IOException, KeyException, JAXBException, SAXException{
		addInput("2");
		addInput("a file");
		addInput("a key");
		endInput();
		UserInterface ui = new UserInterface(voidFactory,voidTimer);
		ui.chooseAlgorithem();
		assertTrue(inContent.available() == 0);
		Mockito.verify(voidFactory,Mockito.only()).getEncryptor(Mockito.anyString(),Mockito.any(),Mockito.any());
		Mockito.verify(voidEncryptor,Mockito.only()).decrypt("a file","a key");
	}
	
	@Test(timeout=1000)
	public void exitWorks() throws IOException, JAXBException, SAXException{
		addInput("7");
		addInput("7");
		UserInterface ui = new UserInterface(voidFactory,voidTimer);
		ui.chooseAlgorithem();
	}
	
	@Test
	public void backFromEncrypt() throws IOException, JAXBException, SAXException{
		addInput("7");
		addInput("1");
		addInput("1");
		addInput("back");
		endInput();
		UserInterface ui = new UserInterface(voidFactory,voidTimer);
		ui.chooseAlgorithem();
		String message = outContent.toString();
		assertEquals(3, numberOfOccurances(message, "ACTION")); 
	}
	
	@Test
	public void backFromDecrypt() throws IOException, JAXBException, SAXException{
		addInput("7");
		addInput("1");
		addInput("2");
		addInput("back");
		endInput();
		UserInterface ui = new UserInterface(voidFactory,voidTimer);
		ui.chooseAlgorithem();
		String message = outContent.toString();
		assertEquals(3, numberOfOccurances(message, "ACTION"));
	}
	
	@Test
	public void decryptDirCallsFactoryDir() throws IOException, JAXBException, SAXException{
		addInput("4");
		endInput();
		UserInterface ui = new UserInterface(voidFactory,voidTimer);
		ui.chooseAlgorithem();
		Mockito.verify(voidFactory).getDirEncryptor(Mockito.any());
	}
	
	@Test
	public void encryptDirCallsFactoryDir() throws IOException, JAXBException, SAXException{
		addInput("3");
		endInput();
		UserInterface ui = new UserInterface(voidFactory,voidTimer);
		ui.chooseAlgorithem();
		Mockito.verify(voidFactory).getDirEncryptor(Mockito.any());
	}
	
	@Test
	public void encryptAsynchDirCallsFactoryDir() throws IOException, JAXBException, SAXException{
		addInput("5");
		endInput();
		UserInterface ui = new UserInterface(voidFactory,voidTimer);
		ui.chooseAlgorithem();
		Mockito.verify(voidFactory).getAsynchDirEncryptor(Mockito.any());
	}
	
	@Test
	public void decryptAsynchDirCallsFactoryDir() throws IOException, JAXBException, SAXException{
		addInput("6");
		endInput();
		UserInterface ui = new UserInterface(voidFactory,voidTimer);
		ui.chooseAlgorithem();
		Mockito.verify(voidFactory).getAsynchDirEncryptor(Mockito.any());
	}
	
	private int numberOfOccurances(String str,String substring){
		int lastIndex = 0;
		int count = 0;
		while(lastIndex != -1){
		    lastIndex = str.indexOf(substring,lastIndex);
		    if(lastIndex != -1){
		        count ++;
		        lastIndex += substring.length();
		    }
		}
		return count;
	}
	
}
