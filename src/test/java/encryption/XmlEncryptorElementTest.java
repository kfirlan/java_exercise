package encryption;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.xml.sax.SAXException;

public class XmlEncryptorElementTest extends TestWithPrint{
	
	@Test
	public void getCorrectEncryptorTest(){
		EncryptorFactory factory = Mockito.mock(EncryptorFactory.class);
		XmlEncryptorElement c = new XmlEncryptorElement(EncryptorFactory.CEASAR_ENCRYPTOR, null, null);
		XmlEncryptorElement r=new XmlEncryptorElement(EncryptorFactory.REVERSE_ENCRYPTOR,c,null);  
		XmlEncryptorElement.toEncryptor(factory, r);
		Mockito.verify(factory).getEncryptor(EncryptorFactory.CEASAR_ENCRYPTOR,
				null, null);
		Mockito.verify(factory).getEncryptor(Mockito.eq(EncryptorFactory.REVERSE_ENCRYPTOR),
				Mockito.any(), Mockito.any());
	}
	
	@Test
	public void seriallizeThenDeseriallizeTest() throws IOException, JAXBException, SAXException{
		XmlEncryptorElement c = new XmlEncryptorElement(EncryptorFactory.CEASAR_ENCRYPTOR, null, null);
		XmlEncryptorElement r=new XmlEncryptorElement(EncryptorFactory.REVERSE_ENCRYPTOR,c,null);  
		String path = folder.newFile("tst.xml").getAbsolutePath();
		r.seriallize(path);
		XmlEncryptorElement res = XmlEncryptorElement.deseriallize(path);
		Assert.assertEquals(r.type, res.type);
		Assert.assertEquals(r.element1.type, c.type);
	}

}
