package encryption;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

public class FileUtilsImp implements FileUtils{
	
	@Override
	public byte[] read(InputStream in) throws IOException {
		byte[] data = IOUtils.toByteArray(in);
		in.close();
		return data;
	}

	@Override
	public void write(OutputStream out, byte[] data) throws IOException {
		out.write(data);
		out.close();
	}
}
