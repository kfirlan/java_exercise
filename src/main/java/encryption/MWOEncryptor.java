package encryption;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import encryption.key.Key;
import encryption.key.KeyException;
import lombok.NonNull;

public class MWOEncryptor extends Encryptor{

	public MWOEncryptor(@NonNull FileUtils fileUtils) {
		super(fileUtils);
	}

	@Override
	public Key generateKey() {
		byte[] res = new byte[1];
		Random r = new Random();
		while(true){
			r.nextBytes(res);
			if (res[0]%2 == 0){ 
				continue;
			}
			return new Key(res[0]);
		}
	}
	
	@Override
	protected byte readKey(Key key) throws KeyException{
		byte tmp = super.readKey(key);
		if (tmp%2 == 0){
			throw new KeyException();
		}
		return tmp;
		
	}
	
	private byte inverse(byte x){
		for (byte i=Byte.MIN_VALUE; i<=Byte.MAX_VALUE;++i){
			if ((byte)(i*x) == 1){
				return i;
			}
		}
		return 0;
	}

	@Override
	public Key encrypt(InputStream in, OutputStream out,Key key) throws IOException {
		return encryptEachByte(in,out,(b,k)->(byte) (b*k),key);
	}

	@Override
	public void decrypt(InputStream in, OutputStream out, Key key) throws IOException, KeyException {
		decryptEachByte(in,out, key, (b,k)->(byte)(b*inverse(k)));
	}

	@Override
	public XmlEncryptorElement toXml() {
		return new XmlEncryptorElement(EncryptorFactory.MWO_ENCRYPTOR, null, null);
	}

}
