package encryption;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import com.google.inject.Inject;

import encryption.key.Key;
import encryption.key.KeyException;

public class UserInterface {
	private static final String illegal_file = "file error try again: ";
	private final static String insert_file = "insert full file path:";
	private final static String insert_key = "insert your key:";
	private final static String illegal_key = "illegal key try different key ";

	private final EncryptorFactory factory;
	@Inject
	public UserInterface(EncryptorFactory factory, TimerListener timerListener) {
		this.factory = factory;
		this.timerListener = timerListener;
	}

	private final TimerListener timerListener;

	private void encrypt(BufferedReader br, Encryptor encryptor) throws IOException {
		System.out.print(insert_file);
		String line = br.readLine();
		Key key;
		while (line != null) {
			if (line.equalsIgnoreCase("back")) {
				return;
			}
			try {
				timerListener.startEvent("encrypt",encryptor);
				key = encryptor.encrypt(line,encryptor.generateKey());
				timerListener.endEvent("encrypt",encryptor);
			} catch (IOException e) {
				System.out.print(illegal_file);
				timerListener.onError(e, encryptor);
				line = br.readLine();
				continue;
			}
			String keyPath = encryptor.getKeyPath(line);
			Key.seriallize(key,keyPath);
			return;
		}
	}

	private void decrypt(BufferedReader br, Encryptor encryptor) throws IOException {
		System.out.print(insert_file);
		String line = br.readLine();
		if (line != null && line.equalsIgnoreCase("back")) {
			return;
		}
		System.out.println(insert_key);
		String key = br.readLine();
		while (line != null && key != null) {
			if (line.equalsIgnoreCase("back")) {
				return;
			}
			if (key.equalsIgnoreCase("back")) {
				return;
			}
			try {
				timerListener.startEvent("decrypt",encryptor);
				encryptor.decrypt(line, key);
				timerListener.endEvent("decrypt",encryptor);
			} catch (IOException e) {
				System.out.print(illegal_file);
				timerListener.onError(e, encryptor);
				line = br.readLine();
				continue;
			} catch (KeyException e) {
				System.out.println(illegal_key);
				timerListener.onError(e, encryptor);
				key = br.readLine();
				continue;
			}
			return;
		}
	}

	private void printMainMenu() {
		System.out.println("|   MENU SELECTION         |");
		System.out.println("============================");
		System.out.println("| Choose Encryption:       |");
		System.out.println("|        1. Ceasar         |");
		System.out.println("|        2. Xor            |");
		System.out.println("|        3. MWO            |");
		System.out.println("|        4. Double         |");
		System.out.println("|        5. Reverse        |");
		System.out.println("|        6. Split          |");
		System.out.println("|        7. Exit           |");
		System.out.println("============================");
	}

	private void printChooseAction() {
		System.out.println("|   CHOOSE ACTION              |");
		System.out.println("================================");
		System.out.println("|        1. Encrypt File       |");
		System.out.println("|        2. Decrypt File       |");
		System.out.println("|        3. Encrypt Dir        |");
		System.out.println("|        4. Decrypt Dir        |");
		System.out.println("|        5. Encrypt Asynch Dir |");
		System.out.println("|        6. Decrypt Asynch Dir |");
		System.out.println("|        7. Change Encryptor   |");
		System.out.println("|        8. Export Encryptor   |");
		System.out.println("================================");
	}

	public void chooseAlgorithem() throws IOException, JAXBException, SAXException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Encryptor encryptor = XmlEncryptorElement.toEncryptor(factory, 
				XmlEncryptorElement.deseriallize(XmlEncryptorElement.getDefaultXml()));
		while (encryptor != null){
			chooseAction(br, encryptor);
			encryptor = getEncryptor(br);
		}
	}

	public Encryptor getEncryptor(BufferedReader br) throws IOException {
		printMainMenu();
		String choice = br.readLine();
		Encryptor encryptor = null;
		while (choice != null) {
			switch (choice) {
			case "1": {
				encryptor = factory.getEncryptor(EncryptorFactory.CEASAR_ENCRYPTOR,null,null);
				return encryptor;
			}
			case "2": {
				encryptor = factory.getEncryptor(EncryptorFactory.XOR_ENCRYPTOR,null,null);
				return encryptor;
			}
			case "3": {
				encryptor = factory.getEncryptor(EncryptorFactory.MWO_ENCRYPTOR,null,null);
				return encryptor;
			}
			case "4": {
				System.out.println("choose first encryptor");
				Encryptor e1 = getEncryptor(br);
				if (e1 == null){
					System.out.println("try another enctyption");
					printMainMenu();
					choice = br.readLine();
					continue;
				}
				System.out.println("choose sedond encryptor");
				Encryptor e2 = getEncryptor(br);
				if (e2 == null){
					System.out.println("try another enctyption");
					printMainMenu();
					choice = br.readLine();
					continue;
				}
				encryptor = factory.getEncryptor(EncryptorFactory.DOUBLE_ENCRYPTOR,e1,e2);
				return encryptor;
			}
			case "5": {
				System.out.println("choose encryptor");
				Encryptor e1 = getEncryptor(br);
				if (e1 == null){
					System.out.println("try another enctyption");
					printMainMenu();
					choice = br.readLine();
					continue;
				}
				encryptor = factory.getEncryptor(EncryptorFactory.REVERSE_ENCRYPTOR,e1,null);
				return encryptor;
			}
			case "6": {
				System.out.println("choose encryptor");
				Encryptor e1 = getEncryptor(br);
				if (e1 == null){
					System.out.println("try another enctyption");
					printMainMenu();
					choice = br.readLine();
					continue;
				}
				encryptor = factory.getEncryptor(EncryptorFactory.SPLIT_ENCRYPTOR,e1,null);
				return encryptor;
			}
			case "7": {
				return null;
			}
			default: {
				System.out.println("Invalid Choice");
				printMainMenu();
				choice = br.readLine();
				continue;
			}
			}
		}
		return encryptor;
	}

	private void chooseAction(BufferedReader br, Encryptor encryptor) throws IOException {
		printChooseAction();
		String choice = br.readLine();
		while (choice != null) {
			switch (choice) {
			case "1": {
				encrypt(br, encryptor);
				break;
			}
			case "2": {
				decrypt(br, encryptor);
				break;
			}
			case "3": {
				encrypt(br, factory.getDirEncryptor(encryptor));
				break;
			}
			case "4": {
				decrypt(br, factory.getDirEncryptor(encryptor));
				break;
			}
			case "5": {
				encrypt(br, factory.getAsynchDirEncryptor(encryptor));
				break;
			}
			case "6": {
				decrypt(br, factory.getAsynchDirEncryptor(encryptor));
				break;
			}
			case "7": {
				return;
			}
			case "8":{
				try {
					encryptor.toXml().seriallize(XmlEncryptorElement.getDefaultXml());
				} catch (JAXBException e) {
					e.printStackTrace();
				}
				break;
			}
			default: {
				System.out.println("Invalid Choice");
			}
			}
			printChooseAction();
			choice = br.readLine();
		}
	}

}
