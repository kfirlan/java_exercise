package encryption.key;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class KeyTest {
	
	@Rule
	public TemporaryFolder folder= new TemporaryFolder();
	
	@Test
	public void seriallizeThenDeseriallize() throws KeyException, IOException{
		Key l = new Key((byte)1);
		Key rl = new Key((byte)2);
		Key rr = new Key((byte)3);
		Key r = new Key(rl,rr);
		Key root = new Key(l,r);
		File file = folder.newFile("1");
		FileOutputStream out = new FileOutputStream(file);
		out.write(Key.seriallize(root));
		out.close();
		Key res = Key.deseriallize(file.getAbsolutePath());
		Key tmp = res.getFirstKey();
		Assert.assertEquals(l.getByteKey(),tmp.getByteKey());
		tmp = res.getSecondKey().getFirstKey();
		Assert.assertEquals(rl.getByteKey(),tmp.getByteKey());
		tmp = res.getSecondKey().getSecondKey();
		Assert.assertEquals(rr.getByteKey(),tmp.getByteKey());
	}
}
