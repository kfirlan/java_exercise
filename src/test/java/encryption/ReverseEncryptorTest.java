package encryption;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Test;
import org.mockito.Mockito;

import encryption.key.Key;
import encryption.key.KeyException;

public class ReverseEncryptorTest extends TestWithPrint {
	
	@Test
	public void decryptOnEncrypt() throws KeyException, IOException{
		FileUtils f = Mockito.mock(FileUtils.class);
		ReverseEncryptor encryptor = new ReverseEncryptor(f);
		Encryptor e1 = Mockito.mock(Encryptor.class);
		InputStream in = Mockito.mock(InputStream.class);
		OutputStream out = Mockito.mock(OutputStream.class);
		Mockito.when(e1.generateKey()).thenReturn(new Key((byte) 1));
		encryptor.setEncryptor(e1);
		Key key = encryptor.generateKey();
		encryptor.encrypt(in,out,key);
		Mockito.verify(e1,Mockito.times(1)).decrypt(in, out, key);
	}
	
	@Test
	public void encryptOnDecrypt() throws KeyException, IOException{
		FileUtils f = Mockito.mock(FileUtils.class);
		ReverseEncryptor encryptor = new ReverseEncryptor(f);
		Encryptor e1 = Mockito.mock(Encryptor.class);
		InputStream in = Mockito.mock(InputStream.class);
		OutputStream out = Mockito.mock(OutputStream.class);
		Key key = Mockito.mock(Key.class);
		encryptor.setEncryptor(e1);
		encryptor.decrypt(in,out,key);
		Mockito.verify(e1,Mockito.times(1)).encrypt(in, out, key);
	}

}
