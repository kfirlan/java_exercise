package encryption;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import encryption.key.KeyException;

public class CeasarEncryptorTest extends TestWithPrint {
	
	@Test
	public void encryptDataReadAndWriteData() throws IOException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		encryptDataReadAndWriteData(CeasarEncryptor.class);
	}
	
	@Test
	public void decryptDataReadAndWriteData() throws IOException, KeyException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		decryptDataReadAndWriteData(CeasarEncryptor.class);
	}
	
	@Test
	public void encryptThenDecryptDoesNothing() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, KeyException{
		encryptThenDecryptDoesNothing(CeasarEncryptor.class);
	}
	
	@Test(expected=KeyException.class)
	public void decryptThrowOnBadKey() throws IOException, KeyException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		decryptThrowOnBadKey(CeasarEncryptor.class);
	}
	
	@Test(expected=IOException.class)
	public void encryptFileThrowOnIllegalFile() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException{
		encryptFileThrowOnIllegalFile(CeasarEncryptor.class);
	}
}
