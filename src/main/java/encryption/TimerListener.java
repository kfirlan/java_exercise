package encryption;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.time.Clock;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.google.inject.Inject;

import lombok.Setter;
import lombok.SneakyThrows;

public class TimerListener {
	
	
	private static final Logger logger = Logger.getLogger(TimerListener.class.getName());
	
	protected final Clock clock;
	
	@Inject
	public TimerListener(Clock clock) {
		this.clock = clock;
	}
	private long start = 0;
	@Setter
	private String logFile = "log_dir.xml"; 
	private Document doc;
	private Element currentEvent;
	private boolean logFileExist = false;
	
	@SneakyThrows
	public void startEvent(String eventName,Encryptor encryptor) {
		String tmp = eventName;
		if (encryptor instanceof DirEncryptor){
			tmp += " directory";
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance(); 
			domFactory.setIgnoringComments(true);
			DocumentBuilder builder = domFactory.newDocumentBuilder(); 
			Element realRoot;
			if (logFileExist){
				doc = builder.parse(new File(logFile));
				realRoot = doc.getDocumentElement();
			}else {
				doc = builder.newDocument(); 
				realRoot = doc.createElement("dir_tries");
				doc.appendChild(realRoot);
				logFileExist = true;
			}
			
			currentEvent = doc.createElement(eventName);
			realRoot.appendChild(currentEvent);
		}
		logger.debug("strated "+tmp);
		System.out.println("started "+tmp);
		start = clock.millis();
	}

	public void endEvent(String eventName,Encryptor encryptor) {
		long time = clock.millis() - start;
		String tmp = eventName;
		if (encryptor instanceof DirEncryptor){
			tmp += " directory";
			currentEvent.setAttribute("status", "success");
			currentEvent.setAttribute("time", Long.toString(time));
			writeDoc();
		}
		logger.debug(tmp+" ended. took: "+time+" millis");
		System.out.println(tmp+" ended. took: "+time+" millis");		
	}
	@SneakyThrows
	public void onError(Exception exception,Encryptor encryptor){
		logger.debug("failed",exception);
		if (! (encryptor instanceof DirEncryptor)){
			return; //dont log non dir operations
		}
		Text type = doc.createTextNode(exception.getClass().toString()); 
		Element eType = doc.createElement("type");
		eType.appendChild(type);
		Text message = doc.createTextNode(exception.getMessage() == null? "":exception.getMessage());
		Element eMessage = doc.createElement("message");
		eMessage.appendChild(message);
		ByteArrayOutputStream tmp = new ByteArrayOutputStream();
		exception.printStackTrace(new PrintStream(tmp));
		Text stackTrace = doc.createTextNode(new String(tmp.toByteArray()));
		Element eStackTrace = doc.createElement("stack_trace");
		eStackTrace.appendChild(stackTrace);
		Element aggregate = doc.createElement("exception");
		aggregate.appendChild(eType);
		aggregate.appendChild(eMessage);
		aggregate.appendChild(eStackTrace);
		currentEvent.appendChild(aggregate);
		currentEvent.setAttribute("status", "fail");
		writeDoc();
	}
	@SneakyThrows
	private void writeDoc(){
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(logFile));
		transformer.transform(source, result);
	}
	
}
