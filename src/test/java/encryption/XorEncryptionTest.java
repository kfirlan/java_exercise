package encryption;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import encryption.key.KeyException;

public class XorEncryptionTest extends TestWithPrint {
	@Test
	public void encryptDataReadAndWriteData() throws IOException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		encryptDataReadAndWriteData(XorEncryptor.class);
	}
	
	@Test
	public void decryptDataReadAndWriteData() throws IOException, KeyException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		decryptDataReadAndWriteData(XorEncryptor.class);
	}
	
	@Test
	public void encryptThenDecryptDoesNothing() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, KeyException{
		encryptThenDecryptDoesNothing(XorEncryptor.class);
	}
	
	@Test(expected=KeyException.class)
	public void decryptThrowOnBadKey() throws IOException, KeyException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		decryptThrowOnBadKey(XorEncryptor.class);
	}
	
	@Test(expected=IOException.class)
	public void encryptFileThrowOnIllegalFile() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException{
		encryptFileThrowOnIllegalFile(XorEncryptor.class);
	}
}
