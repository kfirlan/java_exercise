package encryption;

import static org.junit.Assert.assertArrayEquals;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

import encryption.key.Key;
import encryption.key.KeyException;

public abstract class TestWithPrint {
	protected final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final PipedOutputStream innerInputPipe = new PipedOutputStream();
	protected  PipedInputStream inContent;
	private PrintStream inputPipeWriter = new PrintStream(innerInputPipe); 
	@Rule
	public TemporaryFolder folder= new TemporaryFolder();

	@Before
	public void setUpStreams() throws IOException {
		inContent = new PipedInputStream(innerInputPipe);
		inputPipeWriter = new PrintStream(innerInputPipe); 
	    System.setOut(new PrintStream(outContent));
	    System.setIn(inContent);
	}

	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	    System.setIn(null);
	}
	
	protected void addInput(String s){
		inputPipeWriter.println(s);
	}
	
	protected void endInput(){
		inputPipeWriter.close();
	}
	
	public void encryptDataReadAndWriteData(Class<? extends Encryptor> clazz) throws IOException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Constructor<?> constructor = clazz.getConstructor(FileUtils.class);
		FileUtilsImp f = Mockito.mock(FileUtilsImp.class);
		Mockito.when(f.read(Mockito.any())).thenReturn(new byte[0]);
		Encryptor encryptor = (Encryptor) constructor.newInstance(f);
		encryptor.encrypt(folder.newFile("a").getAbsolutePath(),null);
		Mockito.verify(f,Mockito.times(1)).read(Mockito.any());
		Mockito.verify(f,Mockito.times(1)).write(Mockito.any(),Mockito.any());
	}
	
	public void decryptDataReadAndWriteData(Class<? extends Encryptor> clazz) throws IOException, KeyException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Constructor<?> constructor = clazz.getConstructor(FileUtils.class);
		FileUtilsImp f = Mockito.mock(FileUtilsImp.class);
		Mockito.when(f.read(Mockito.any())).thenReturn(new byte[0]);
		Encryptor encryptor = (Encryptor) constructor.newInstance(f);
		encryptor.decrypt(folder.newFile("a").getAbsolutePath(),createKeyFile("11"));
		Mockito.verify(f,Mockito.times(1)).read(Mockito.any());
		Mockito.verify(f,Mockito.times(1)).write(Mockito.any(),Mockito.any());
	}
	
	public void decryptThrowOnBadKey(Class<? extends Encryptor> clazz) throws IOException, KeyException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Constructor<?> constructor = clazz.getConstructor(FileUtils.class);
		FileUtilsImp f = Mockito.mock(FileUtilsImp.class);
		Encryptor encryptor = (Encryptor) constructor.newInstance(f);
		encryptor.decrypt(folder.newFile("a").getAbsolutePath(),"asd");
	}
	
	public void encryptThenDecryptDoesNothing(Class<? extends Encryptor> clazz) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, KeyException{
		Constructor<?> constructor = clazz.getConstructor(FileUtils.class);
		MockFileUtils f = new MockFileUtils();
		Encryptor encryptor = (Encryptor) constructor.newInstance(f);
		File file = folder.newFile("a");
		Key key = encryptor.encrypt(file.getAbsolutePath(),null);
		encryptor.decrypt(file.getAbsolutePath(), createKeyFile(Byte.toString(key.getByteKey())));
		assertArrayEquals(f.data, f.tmp.get(2));
	}
	
	
	public void encryptFileThrowOnIllegalFile(Class<? extends Encryptor> clazz) throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		Constructor<?> constructor = clazz.getConstructor(FileUtils.class);
		MockFileUtils f = new MockFileUtils();
		Encryptor encryptor = (Encryptor) constructor.newInstance(f);
		File file= folder.newFile("a");
		encryptor.encrypt(file.getAbsolutePath()+"ab",null);
	}
	
	private static class MockFileUtils implements FileUtils{
		public byte[] data = new byte[20];
		public final ArrayList<byte[]> tmp = new ArrayList<>();
		private int i=0;
		
		public MockFileUtils() {
			new Random().nextBytes(data);
			tmp.add(data);
		}
		@Override
		public byte[] read(InputStream in) throws IOException {
			byte[] res = tmp.get(i).clone();
			++i;
			return res;
		}
		@Override
		public void write(OutputStream out, byte[] data) throws IOException {
			tmp.add(data);
			
		}
	}
	
	protected String createKeyFile(String keyfile) throws IOException{
		File tmp = folder.newFile(keyfile);
		Key key = new Key((byte) Integer.parseInt(keyfile));
		key.seriallize(key, tmp.getAbsolutePath());
		return tmp.getAbsolutePath();
	}
}
