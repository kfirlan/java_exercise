package encryption;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Test;
import org.mockito.Mockito;

import encryption.SplitEncryptor.Pair;
import encryption.key.Key;
import encryption.key.KeyException;

import static org.junit.Assert.*;
public class SplitEncryptorTest extends TestWithPrint{
	@Test
	public void splitThenMerge() throws IOException{
		FileUtils fileUtils = Mockito.mock(FileUtils.class);
		SplitEncryptor encryptor = new SplitEncryptor(fileUtils);
		byte [] data = {1,2,3,4,5,6,7,8,9,0};
		Pair<Byte[],Byte[]> pair = encryptor.split(data);
		
		byte [] merged = encryptor.merge(ByteArrToOutputStream(pair.getElement1())
				, ByteArrToOutputStream(pair.getElement2()));
		assertArrayEquals(data, merged);
	}
	
	@Test
	public void encrypCalledTwice() throws KeyException, IOException{
		FileUtils fileUtils = Mockito.mock(FileUtils.class);
		SplitEncryptor encryptor = new SplitEncryptor(fileUtils);
		Encryptor inner = Mockito.mock(Encryptor.class);
		Mockito.when(inner.generateKey()).thenReturn(new Key((byte)1));
		encryptor.setEncryptor(inner);
		InputStream in = Mockito.mock(InputStream.class);
		OutputStream out = Mockito.mock(OutputStream.class);
		encryptor.encrypt(in, out,encryptor.generateKey());
		Mockito.verify(inner,Mockito.times(2)).encrypt(Mockito.any(), Mockito.any(), Mockito.any());
	}
	
	@Test
	public void decryptCalledTwice() throws KeyException, IOException{
		FileUtils fileUtils = Mockito.mock(FileUtils.class);
		SplitEncryptor encryptor = new SplitEncryptor(fileUtils);
		Encryptor inner = Mockito.mock(Encryptor.class);
		Mockito.when(inner.generateKey()).thenReturn(new Key((byte)1));
		encryptor.setEncryptor(inner);
		InputStream in = Mockito.mock(InputStream.class);
		OutputStream out = Mockito.mock(OutputStream.class);
		encryptor.decrypt(in, out,encryptor.generateKey());
		Mockito.verify(inner,Mockito.times(2)).decrypt(Mockito.any(), Mockito.any(), Mockito.any());
	}
	
	private ByteArrayOutputStream ByteArrToOutputStream(Byte[] data) throws IOException{
		byte [] tmp = new byte[data.length];
		for (int i = 0; i < tmp.length; i++) {
			tmp[i] = data[i];
		}
		ByteArrayOutputStream res = new ByteArrayOutputStream();
		res.write(tmp);
		return res;
	}
}
