package encryption;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import encryption.key.Key;
import encryption.key.KeyException;
import lombok.Data;
import lombok.Setter;

@Setter
/*
 * even or odd refer to the index of the byte in the array.
 * the first byte is even because it index is 0
 */
public class SplitEncryptor extends Encryptor {
	
	private Encryptor encryptor;

	public SplitEncryptor(FileUtils fileUtils) {
		super(fileUtils);
	}

	@Override
	public Key encrypt(InputStream in, OutputStream out, Key key) throws IOException, KeyException {
		byte[] data = fileUtils.read(in);
		Pair<Byte[],Byte[]> pair = split(data);
		InputStream evenInput = ByteArrToInputStream(pair.getElement1());
		InputStream oddInput = ByteArrToInputStream(pair.getElement2());
		ByteArrayOutputStream evenOut = new ByteArrayOutputStream();
		ByteArrayOutputStream oddOut = new ByteArrayOutputStream();
		encryptor.encrypt(evenInput, evenOut,key.getFirstKey());
		encryptor.encrypt(oddInput, oddOut,key.getSecondKey());
		data = merge(evenOut,oddOut);
		fileUtils.write(out, data);
		return key;
	}

	@Override
	public void decrypt(InputStream in, OutputStream out, Key key) throws IOException, KeyException {
		byte[] data = fileUtils.read(in);
		Pair<Byte[],Byte[]> pair = split(data);
		InputStream evenInput = ByteArrToInputStream(pair.getElement1());
		InputStream oddInput = ByteArrToInputStream(pair.getElement2());
		ByteArrayOutputStream evenOut = new ByteArrayOutputStream();
		ByteArrayOutputStream oddOut = new ByteArrayOutputStream();
		encryptor.decrypt(evenInput, evenOut,key.getFirstKey());
		encryptor.decrypt(oddInput, oddOut,key.getSecondKey());
		data = merge(evenOut,oddOut);
		fileUtils.write(out, data);
	}

	@Override
	public Key generateKey() {
		Key k1  = encryptor.generateKey();
		Key k2 = encryptor.generateKey();
		return new Key(k1,k2);
	}
	
	public Pair<Byte[],Byte[]> split(byte[] data){
		ArrayList<Byte> even = new ArrayList<>();
		ArrayList<Byte> odd = new ArrayList<>();
		if (data != null){
			for (int i = 0; i < data.length; i++) {
				if (i%2 == 0){
					even.add(data[i]);
					continue;
				}
				odd.add(data[i]);
			}
		}
		Byte []oddArr = new Byte[odd.size()];
		oddArr = odd.toArray(oddArr);
		Byte []evenArr = new Byte[even.size()];
		evenArr = even.toArray(evenArr);
		return new Pair<Byte[],Byte[]>(evenArr,oddArr);
	}
	
	public InputStream ByteArrToInputStream(Byte[] data){
		byte[] arr = new byte[data.length];
		for (int i = 0; i < arr.length; i++) {
			arr[i]= data[i];
		}
		return new ByteArrayInputStream(arr);
	}
	
	public byte[] merge (ByteArrayOutputStream even,ByteArrayOutputStream odd){
		byte [] evenBytes = even.toByteArray();
		byte [] oddBytes = odd.toByteArray();
		byte [] res = new byte[evenBytes.length+oddBytes.length];
		for (int i=0; i<res.length; ++i){
			if (i%2 == 0){
				res[i] = evenBytes[i/2];
				continue;
			}
			res[i] = oddBytes[i/2];
		}
		return res;
	}
	
	@Data
	public static class Pair<U,V>{
		private final U element1;
		private final V element2;
	}

	@Override
	public XmlEncryptorElement toXml() {
		return new XmlEncryptorElement(EncryptorFactory.SPLIT_ENCRYPTOR, encryptor.toXml(), null);
	}

}
