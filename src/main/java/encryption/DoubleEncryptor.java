package encryption;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import encryption.key.Key;
import encryption.key.KeyException;
import lombok.Setter;

@Setter
public class DoubleEncryptor extends Encryptor {
	public DoubleEncryptor(FileUtils fileUtils) {
		super(fileUtils);
	}
	private Encryptor encryptor1;
	private Encryptor encryptor2;
	@Override
	public Key encrypt(String file,Key key) throws IOException {
		InputStream in = new FileInputStream(file);
		OutputStream out = new FileOutputStream(getEncryptedName(file));
		return encrypt(in, out,key);
	}
	
	@Override
	public Key generateKey() {
		Key key1 = encryptor1.generateKey();
		Key key2 = encryptor2.generateKey();
		return new Key(key1,key2);
	}
	@Override
	public Key encrypt(InputStream in, OutputStream out,Key key) throws IOException {
		PipedOutputStream out1 = new PipedOutputStream();
		PipedInputStream in2 = new PipedInputStream(out1);
		encryptor1.encrypt(in, out1,key.getFirstKey());
		encryptor2.encrypt(in2,out,key.getSecondKey());
		return key;
	}
	@Override
	public void decrypt(InputStream in, OutputStream out, Key key) throws IOException, KeyException {
		PipedOutputStream out2 = new PipedOutputStream();
		PipedInputStream in1 = new PipedInputStream(out2);
		encryptor2.decrypt(in, out2, key.getSecondKey());
		encryptor1.decrypt(in1, out, key.getFirstKey());
	}

	@Override
	public XmlEncryptorElement toXml() {
		return new XmlEncryptorElement(EncryptorFactory.DOUBLE_ENCRYPTOR, encryptor1.toXml(), encryptor2.toXml());
	}
	
}
