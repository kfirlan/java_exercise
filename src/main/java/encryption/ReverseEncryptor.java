package encryption;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import encryption.key.Key;
import encryption.key.KeyException;
import lombok.Setter;

@Setter
public class ReverseEncryptor extends Encryptor {
	
	private Encryptor encryptor;
	
	public ReverseEncryptor(FileUtils fileUtils) {
		super(fileUtils);
	}

	@Override
	public Key encrypt(InputStream in, OutputStream out,Key key) throws IOException, KeyException {
		encryptor.decrypt(in, out, key);
		return key;
	}

	@Override
	public void decrypt(InputStream in, OutputStream out, Key key) throws IOException, KeyException {
		encryptor.encrypt(in, out, key);		
	}

	@Override
	public Key generateKey() {
		return encryptor.generateKey();
	}

	@Override
	public XmlEncryptorElement toXml() {
		return new XmlEncryptorElement(EncryptorFactory.REVERSE_ENCRYPTOR, encryptor.toXml(), null);
	}

}
